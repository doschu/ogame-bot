import sys
import os
from PySide2 import QtXml
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QPushButton, QLineEdit, QShortcut
from PySide2.QtGui import QKeySequence
from PySide2.QtCore import QFile
from botthread import BotThread
from config import options

def spawn_bot():
    
    if ui.checkBox_rememberMe.isChecked():
        options.set('credentials','user', user.text())
        options.set('credentials', 'pass', password.text())
        options.set('credentials','server', server.currentText())
        options.set('general','remember_me','1')

    bot = BotThread(user.text(), password.text(),server.currentText())
    bot.start()

def set_server_list(comboBox):
    comboBox.addItem('ar')
    comboBox.addItem('br')
    comboBox.addItem('cz')
    comboBox.addItem('de')
    comboBox.addItem('en')
    comboBox.addItem('es')
    comboBox.addItem('fi')
    comboBox.addItem('fr')
    comboBox.addItem('gr')
    comboBox.addItem('hr')
    comboBox.addItem('hu')
    comboBox.addItem('it')
    comboBox.addItem('jp')
    comboBox.addItem('mx')
    comboBox.addItem('nl')
    comboBox.addItem('pl')
    comboBox.addItem('pt')
    comboBox.addItem('ro')
    comboBox.addItem('ru')
    comboBox.addItem('se')
    comboBox.addItem('si')
    comboBox.addItem('sk')
    comboBox.addItem('tr')
    comboBox.addItem('tw')
    comboBox.addItem('us')
    comboBox.addItem('yu')


if __name__ == "__main__":

    if getattr(sys, 'frozen', False):
        application_path = sys._MEIPASS + "\\"
    else:
        application_path = os.path.dirname(os.path.abspath(__file__)) + '/'

    app = QApplication(sys.argv)

    file = QFile(application_path + 'main.ui')
    file.open(QFile.ReadOnly)

    loader = QUiLoader()
    ui = loader.load(file)

    startButton = ui.pushButton_start
    startButton.clicked.connect(spawn_bot)
    startButton.setShortcut(QKeySequence('Return'))

    user = ui.lineEdit_user
    password = ui.lineEdit_pass
    server = ui.comboBox_server
    set_server_list(server)
    
    if options['general']['remember_me'] == '0':
        ui.checkBox_rememberMe.setChecked(False)
    else:
        ui.checkBox_rememberMe.setChecked(True)
        user.setText(options['credentials']['user'])
        password.setText(options['credentials']['pass'])
        index = server.findText(options['credentials']['server'])
        if index != -1:
            server.setCurrentIndex(index)

    ui.show()

    sys.exit(app.exec_())
