import time
import re
from sim import Sim
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from ship import Ship
from clicker import scroll_click
from coordinates import Coordinates

class Planet():
    def __init__(self, planet_id, driver, logger, options):
        self.planet_id = planet_id
        self.planet_name = None
        self.driver = driver
        self.logger = logger
        self.options = options
        self.planet_coords = None
        self.planet_resources = {} 
        self.planet_structures = {}
        self.planet_mines = {}
        self.planet_stations = {}
        self.researches = {}
        self.planet_defenses = None
        self.planet_fleet = {} 
        self.moon = None
        self.moon_fleet = None
        self.sim = Sim()
        self.actions = ActionChains(self.driver)

        self.get_planet_name()
        self.get_planet_coords()
        self.has_moon()
        
    def upgrade_resources(self):
        if self.options['building']['enabled'] == '1':
            self.navigate_to_planet('resources')
            self.logger.info('Updating planet %s resource mines' % (self.planet_name+' ['+str(self.planet_coords)+']'))
            #check building queue
            if self.is_queue_empty('resources'):
                #needs check below this point
                if not self.should_upgrade_storage():
                    #get mine levels
                    self.get_mines_status()
                    #decide next resource upgrade
                    self.choose_mine_upgrade()
            else:
                self.logger.info('Building Queue is NOT empty')
    
    def get_mines_status(self):
        structure = {'name': 'metalMine', 'buttonid':'button1', 'type':'resources'}
        mine_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure['energyneeded'] = self.sim.upgrade_energy_cost(structure['name'],structure['level']+1)
        if mine_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0
        self.planet_structures[structure['name']] = structure

        structure = {'name':'crystalMine', 'buttonid':'button2','type':'resources'}
        mine_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure['energyneeded'] = self.sim.upgrade_energy_cost(structure['name'],structure['level']+1)
        if mine_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0
        self.planet_structures[structure['name']] = structure

        structure = {'name':'deuteriumMine', 'buttonid':'button3','type':'resources'}
        mine_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure['energyneeded'] = self.sim.upgrade_energy_cost(structure['name'],structure['level']+1)
        if mine_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0
        self.planet_structures[structure['name']] = structure

        structure = {'name':'solarPlant', 'buttonid':'button4','type':'resources'}
        mine_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure['energyneeded'] = 0
        if mine_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0
        self.planet_structures[structure['name']] = structure

        structure = {'name':'fusionPlant', 'buttonid':'button5','type':'resources'}
        mine_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure['energyneeded'] = 0
        if mine_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0
        self.planet_structures[structure['name']] = structure
    
    def choose_mine_upgrade(self):
        if int(self.options['building']['min_energy_level']) > self.planet_resources['energy']:
            self.upgrade_energy()

        else:
            if int(self.options['building']['levels_diff'][2]) < (self.planet_structures['metalMine']['level'] - self.planet_structures['deuteriumMine']['level']):
                if self.planet_structures['deuteriumMine']['upgradeable'] == 1:
                    if self.planet_structures['deuteriumMine']['energyneeded'] < (self.planet_resources['energy'] - int(self.options['building']['min_energy_level'])):
                        self.upgrade(self.planet_structures['deuteriumMine'])
                    else:
                        self.upgrade_energy()

            if int(self.options['building']['levels_diff'][0]) < (self.planet_structures['metalMine']['level'] - self.planet_structures['crystalMine']['level']):
                if self.planet_structures['crystalMine']['upgradeable'] == 1:
                    if self.planet_structures['crystalMine']['energyneeded'] < (self.planet_resources['energy'] - int(self.options['building']['min_energy_level'])):
                        self.upgrade(self.planet_structures['crystalMine'])
                    else:
                        self.upgrade_energy()

            if self.planet_structures['metalMine']['upgradeable'] == 1:
                if self.planet_structures['metalMine']['energyneeded'] < (self.planet_resources['energy'] - int(self.options['building']['min_energy_level'])):
                    self.upgrade(self.planet_structures['metalMine'])
                else:
                    self.upgrade_energy()

    
    def upgrade(self, structure):
        if self.is_queue_empty(structure['type']):
            if structure['type'] is 'research':
                button_element =  self.driver.find_element_by_xpath("//div[contains(@class, '"+structure['buttonid']+"')]")
            else:
                button_element = self.driver.find_element_by_id(structure['buttonid'])
            upgrade_button_element = button_element.find_element_by_class_name("fastBuild.tooltip.js_hideTipOnMobile")
            self.scroll_shim(upgrade_button_element)
            scroll_click(self.driver, upgrade_button_element)
            self.logger.info('Upgrading %s to level %s' % (structure['name'],structure['level']+1))

    def scroll_shim(self, element):
        x = element.location['x']
        y = element.location['y']
        scroll_by_coord = 'window.scrollTo(%s,%s);' % (
            x,
            y
        )
        scroll_nav_out_of_way = 'window.scrollBy(0, -120);'
        self.driver.execute_script(scroll_by_coord)
        self.driver.execute_script(scroll_nav_out_of_way)

    def upgrade_energy(self):
        if self.planet_structures['solarPlant']['upgradeable'] == 1:
            self.upgrade(self.planet_structures['solarPlant'])
        elif self.planet_structures['fusionPlant']['upgradeable'] == 1:
            if int(self.options['building']['max_fusion_plant_level']) > self.planet_structures['fusionPlant']['level']:
                self.upgrade(self.planet_structures['fusionPlant'])

    
    def get_storage_status(self):
        structure = {'name':'metalStorage', 'buttonid': 'button7','type':'resources'}
        storage_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(storage_element.find_element_by_class_name("level").text)
        structure['capacity'] = int(self.sim.get_capacity(structure['level']))
        structure['free'] = max(int(structure['capacity']) - int(self.planet_resources['metal']),0)
        if self.planet_resources['metal'] > structure['capacity']:
            structure['percent_free'] = 0
        else:
            structure['percent_free'] = 100-(self.planet_resources['metal']/structure['capacity']*100)

        if storage_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.planet_structures[structure['name']] = structure

        structure = {'name':'crystalStorage', 'buttonid': 'button8','type':'resources'}
        storage_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(storage_element.find_element_by_class_name("level").text)
        structure['capacity'] = int(self.sim.get_capacity(structure['level']))
        structure['free'] = max(int(structure['capacity']) - int(self.planet_resources['crystal']),0)
        if self.planet_resources['crystal'] > structure['capacity']:
            structure['percent_free'] = 0
        else:
            structure['percent_free'] = 100-(self.planet_resources['crystal']/structure['capacity']*100)
        if storage_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.planet_structures[structure['name']] = structure

        structure = {'name':'deuteriumStorage', 'buttonid': 'button9','type':'resources'}
        storage_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(storage_element.find_element_by_class_name("level").text)
        structure['capacity'] = int(self.sim.get_capacity(structure['level']))
        structure['free'] = max(int(structure['capacity']) - int(self.planet_resources['deuterium']),0)
        if self.planet_resources['deuterium'] > structure['capacity']:
            structure['percent_free'] = 0
        else:
            structure['percent_free'] = 100-(self.planet_resources['deuterium']/structure['capacity']*100)

        if storage_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0
        self.planet_structures[structure['name']] = structure


    def choose_storage_upgrade(self):
       if self.planet_structures['metalStorage']['percent_free'] < int(self.options['building']['min_storage_free'].split(',')[0]):
            self.upgrade(self.planet_structures['metalStorage'])
            return True
       elif self.planet_structures['crystalStorage']['percent_free'] < int(self.options['building']['min_storage_free'].split(',')[1]):
            self.upgrade(self.planet_structures['crystalStorage'])
            return True
       elif self.planet_structures['deuteriumStorage']['percent_free'] < int(self.options['building']['min_storage_free'].split(',')[2]):
            self.upgrade(self.planet_structures['deuteriumStorage'])
            return True
       else:
            return False


    def should_upgrade_storage(self):
        self.navigate_to_planet('resources')
        self.get_storage_status()
        return self.choose_storage_upgrade()
    
    def upgrade_stations(self):
        if self.options['station']['enabled'] == '1':
            self.logger.info("Updating planet %s stations" % (self.planet_name+' ['+str(self.planet_coords)+']'))
            self.navigate_to_planet('station')
            if self.is_queue_empty('station'):
                self.choose_station_upgrade()
            else:
                self.logger.info("Building Queue is NOT empty")


    def choose_station_upgrade(self):
        self.get_stations_status()
        if self.planet_structures['roboticsFactory']['upgradeable'] == 1:
            self.upgrade(self.planet_structures['roboticsFactory'])
        elif self.planet_structures['shipyard']['upgradeable'] == 1:
            self.upgrade(self.planet_structures['shipyard'])
        elif self.planet_structures['researchLab']['upgradeable'] == 1:
            self.upgrade(self.planet_structures['researchLab'])
        elif self.planet_structures['naniteFactory']['upgradeable'] == 1:
            self.upgrade(self.planet_structures['naniteFactory'])
        else:
            self.logger.info('Not enough resources to upgrade stations') 

    def get_stations_status(self):
        structure = {'name':'roboticsFactory', 'buttonid': 'button0','type':'station'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(structure_element.find_element_by_class_name("level").text)
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0 

        self.planet_structures[structure['name']] = structure

        structure ={'name':'shipyard', 'buttonid': 'button1','type':'station'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(structure_element.find_element_by_class_name("level").text)
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.planet_structures[structure['name']] = structure

        structure ={'name':'researchLab', 'buttonid': 'button2','type':'station'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(structure_element.find_element_by_class_name("level").text)
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.planet_structures[structure['name']] = structure

        structure ={'name':'naniteFactory', 'buttonid': 'button5','type':'station'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(structure_element.find_element_by_class_name("level").text)
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.planet_structures[structure['name']] = structure
    
    def get_research_status(self):
        self.driver.switch_to.default_content()
        research = {'name':'energyTech', 'buttonid': 'research113', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'laserTech', 'buttonid': 'research120', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'ionTech', 'buttonid': 'research121', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'hyperspaceTech', 'buttonid': 'research114', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'plasmaTech', 'buttonid': 'research122', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'combustionDrive', 'buttonid': 'research115', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'impulseDrive', 'buttonid': 'research117', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'hyperspaceDrive', 'buttonid': 'research118', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'espionageTech', 'buttonid': 'research106', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'computerTech', 'buttonid': 'research108', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'astrophysics', 'buttonid': 'research124', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'intergalacticRes', 'buttonid': 'research123', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'gravitonTech', 'buttonid': 'research199', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'weaponTech', 'buttonid': 'research109', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'shieldingTech', 'buttonid': 'research110', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research


        research = {'name':'armourTech', 'buttonid': 'research111', 'type':'research'}
        research_element = self.driver.find_element_by_xpath("//div[contains(@class, '"+research['buttonid']+"')]")
        if "fastBuild" in research_element.get_attribute("outerHTML"):
            research['upgradeable'] = 1
        else:
            research['upgradeable'] = 0

        research['level'] = int(research_element.find_element_by_class_name('level').text.split(' ')[0])

        self.researches[research['name']] = research

    def choose_research_upgrade(self):
        self.get_research_status()
        upgraded = False
        priority_list = self.options['research']['high_priority'].split(',') + self.options['research']['medium_priority'].split(',') + self.options['research']['low_priority'].split(',')
        for research in priority_list:
            if self.researches[research]['upgradeable'] == 1:
                self.upgrade(self.researches[research])
                upgraded = True
                break

        if not upgraded:
            self.logger.info("Not enough resources to upgrade research")


    def upgrade_researchs(self):
        if self.options['research']['enabled'] == '1':
            self.logger.info("Updating researchs")
            self.navigate_to_planet('research')
            if self.is_queue_empty('research'):
                self.choose_research_upgrade()
            else:
                self.logger.info("Research Queue is NOT empty")


    
    def build_ships(self):
        self.navigate_to_planet('shipyard')
    
    def build_defenses(self):
        self.navigate_to_planet('defense')
    
    #it needs to improve the detection of the element warning_element
    def is_under_attack(self):
        warning_element = self.driver.find_element_by_id('attack_alert')
        if ((warning_element.get_attribute('class') == 'tooltip eventToggle soon') or 
            (warning_element.get_attribute('class') == 'tooltip eventToggle today')):
            print("detected attack")
            try:
                expander_element = self.driver.find_element_by_id('js_eventDetailsClosed')
            except:
                pass
            expander_element = scroll_click(self.driver, expander_element)
            fleet_movement_element = self.driver.find_elements_by_class_name('eventFleet')
            attacks_list = []
            for fleet_row in fleet_movement_element:
                print("for")
                move_type = fleet_row.get_attribute('data-mission-type')
                if move_type == '1' or move_type == '2' or move_type == '9' or move_type == '6':
                    print("if")
                    attack = {}
                    attack['id'] = int(fleet_row.get_attribute('id').split('-')[1])
                    attack['origin_name'] = fleet_row.find_element_by_class_name('originFleet').text
                    attack['origin_coords'] = Coordinates(fleet_row.find_element_by_class_name('coordsOrigin').text)
                    attack['dest_name'] = fleet_row.find_element_by_class_name('destFleet').text
                    attack['dest_coords'] = Coordinates(fleet_row.find_element_by_class_name('destCoords').text)
                    current_id = fleet_row.get_attribute('id').split('-')[1]
                    attack['time_left'] = self.transform_to_seconds(fleet_row.find_element_by_id('counter-eventlist-'+current_id).text)
                    attacks_list.append(attack)

            return attacks_list

    #This should be improved, its a quick fix that only allow to fleet save 24 hours until arrive
    def transform_to_seconds(self, time):
        all_data = time.split(' ')
        print(time)
        multiplier = 1
        result = 0
        for index,data in enumerate(all_data):
            if(index < 3):
                result += int(re.sub('[^0-9]','',data)) * multiplier
                multiplier = multiplier * 60
            else:
                result = 999999999999999
                break
        print("result:" + str(result))
        return result

    def update_fleet(self):
        if self.options['fleet']['enabled'] == '1': 
            self.logger.info("Updating planet fleet")
            self.navigate_to_planet('fleet1')
            self.get_fleet()

    def send_fleet(self,destination,mission_type,speed,all_fleet=False,all_resources=False):
    #def send_fleet(self,destination,mission_type,speed,resources,fleet, all_fleet=False, all_resources=False):
        #---------------
        # MISSION TYPES:
        # 1- attack
        # 2- group attack
        # 3- transport
        # 4- deploy
        # 5- hold postion
        # 6- spy
        # 7- colonize
        # 8- recicle debris
        # 9- destroy
        # 15- expedition
        #---------------
        self.navigate_to_planet('fleet1')
        #check if there is any fleet
        try:
            warning_element = self.driver.find_element_by_id('warning')
        except NoSuchElementException:
            if not all_fleet:
                pass
                #for ship in fleet:
                    #self.driver.find_element_by_id('ship_'+str(ship.id)).send_keys(ship.amount)
            else:
                send_all_element = self.driver.find_element_by_id('sendall')
                scroll_click(self.driver, send_all_element)

            continue_element = self.driver.find_element_by_id('continue') #next window
            scroll_click(self.driver, continue_element)
            
            
            galaxy_element = self.driver.find_element_by_id('galaxy')
            scroll_click(self.driver, galaxy_element)
            galaxy_element.send_keys(destination[0])
            
            system_element = self.driver.find_element_by_id('system')
            scroll_click(self.driver, system_element)
            system_element.clear()
            system_element.send_keys(destination[1])

            position_element = self.driver.find_element_by_id('position')
            scroll_click(self.driver, position_element)
            position_element.clear()
            position_element.send_keys(destination[2])
            
            speed_elements = self.driver.find_elements_by_xpath("//a[contains(@class, 'dark_highlight_tablet')]")
            for speed_element in speed_elements:
                if speed_element.text == str(speed):
                    scroll_click(self.driver, speed_element)
                    break

            continue_element = self.driver.find_element_by_id('continue') #next window
            scroll_click(self.driver, continue_element)
            
            #mission type
            mission_button = self.driver.find_element_by_id('button'+str(mission_type))
            scroll_click(self.driver, mission_button)
            
            if not all_resources:
                pass
                #self.driver.find_element_by_id('metal').send_keys(resources['metal'])
                #self.driver.find_element_by_id('crystal').send_keys(resources['crystal'])
                #self.driver.find_element_by_id('deuterium').send_keys(resources['deuterium'])
            else:
                all_resources_element = self.driver.find_element_by_id('allresources')
                scroll_click(self.driver, all_resources_element)

            send_element = self.driver.find_element_by_id('start') #send the fleet
            scroll_click(self.driver, send_element)

    def get_fleet(self):
        fleet_detected = False
        try:
            warning_element = self.driver.find_element_by_id('warning')
            self.logger.info("No fleet found")
        except NoSuchElementException:

            for x in range(202, 216):
                try:
                    ship_element = self.driver.find_element_by_id('button'+str(x))
                    ship_amount = ship_element.find_element_by_class_name('level').text
                    fleet_detected = True
                    if int(ship_amount) > 0:
                        if x not in self.planet_fleet:
                            self.planet_fleet[x] = (Ship(x,ship_amount))
                        else:
                            self.planet_fleet[x].amount = ship_amount
                except TimeoutException:
                    pass

                except NoSuchElementException:
                    pass
                
       
    def get_nearest_planet(self, planet_coords):
        pass

        
    def navigate_to_planet(self,tab=None):
        try:
            active_element = self.driver.find_element_by_xpath("//a[contains(@class, 'active')]")
            active_id = active_element.get_attribute("href")
            if  not self.planet_id.split('-')[1] in active_id:
                planet_element = self.driver.find_element_by_id(self.planet_id)
                scroll_click(self.driver, planet_element)
                time.sleep(4)#maybe needs to wait until page has been fully loaded

            if tab is not None:
                if tab not in self.driver.current_url: 
                    tab_element = self.driver.find_element_by_xpath("//a[contains(@href, '"+tab+"')]")
                    scroll_click(self.driver, tab_element)
        except:
            pass

    def get_planet_name(self):
        planet_element = self.driver.find_element_by_xpath("//div[contains(@id, '"+self.planet_id+"')]")
        self.planet_name = planet_element.find_element_by_class_name('planet-name').text
        
    def get_planet_coords(self):
        planet_element = self.driver.find_element_by_xpath("//div[contains(@id, '"+self.planet_id+"')]")
        self.planet_coords = Coordinates(planet_element.find_element_by_class_name('planet-koords').text)
        
    def has_moon(self):
        planet_element = self.driver.find_element_by_xpath("//div[contains(@id, '"+self.planet_id+"')]")
        moon_element = planet_element.find_element_by_xpath('//a[contains(@class, moonlink)]')
        if moon_element is None:
            self.moon = False
        else:
            self.moon = True
    
    def get_planet_resources(self):
        self.logger.info('Updating planet %s info' % (self.planet_name+'  ['+str(self.planet_coords)+']'))
        self.navigate_to_planet()
        #first update the available resources
        self.planet_resources["metal"] = int(self.driver.find_element_by_id("resources_metal").text.replace(".",""))
        self.planet_resources["crystal"] = int(self.driver.find_element_by_id("resources_crystal").text.replace(".",""))
        self.planet_resources["deuterium"] = int(self.driver.find_element_by_id("resources_deuterium").text.replace(".",""))
        self.planet_resources["energy"] = int(self.driver.find_element_by_id("resources_energy").text.replace(".",""))
        self.logger.info(self.planet_resources)
        
    def is_queue_empty(self,tab):
        self.navigate_to_planet(tab)
        
        queue_element = self.driver.find_element_by_class_name('content-box-s')
        element = queue_element.find_elements_by_class_name('data')
        if len(element) > 0: #if the data element appears the queue is not empty
            return False
        else:
            return True
