import time
from sim import Sim
from planet import Planet
from ship import Ship

class Worker():
    
    def __init__(self, driver,workertab,logger,options):
        self.sim = Sim()
        self.driver = driver
        self.worker_tab = workertab
        self.logger = logger
        self.options = options
        self.planets = []
        self.planets_coords = []
        self.resources = {}
        self.structures = {}
        self.station_structures = {}
        self.researches = {}
        self.fleet_saved = []

    def fetch_planets(self):
        planet_list = self.driver.find_elements_by_xpath("//div[contains(@id, 'planet-')]")
        for planet in planet_list:
            self.planets.append(Planet(planet.get_attribute("id"),self.driver,self.logger,self.options))
            self.planets_coords.append(self.planets[-1].planet_coords)


    def update_planet_fleet(self, planet):
        self.logger.info("UPDATING PLANET FLEET")

    def update_planet_stations(self, planet):
        if self.options['station']['enabled'] == '1':
            self.logger.info("Updating planet stations")
            self.driver.find_element_by_xpath("//a[contains(@href, 'station')]").click()
            if self.is_building_queue_empty():
                self.choose_station_upgrade()
            else:
                self.logger.info("Building Queue is NOT empty")

    def update_planet_researches(self, planet):
        if self.options['research']['enabled'] == '1':
            self.logger.info("Updating researchs")
            self.driver.find_element_by_xpath("//a[contains(@href, 'research')]").click()
            if self.is_research_queue_empty():
                self.choose_research_upgrade()
            else:
                self.logger.info("Research Queue is NOT empty")

    def update_planet_hangar(self, planet):
        self.logger.info("UPDATING PLANET HANGAR QUEUE")

    def update_planet_defenses(self, planet):
        self.logger.info("UPDATING PLANET DEFENSE QUEUE")

    def handle_planets(self):
        self.fetch_planets()

        for p in iter(self.planets):
            #returns all attacks in all planets in this universe
            all_attacks = p.is_under_attack()
            print(all_attacks)
            if self.options['fleet']['enabled'] == '1':
                if all_attacks is not None:
                    disregarded_attacks = self.unfleetsaved_attacks(all_attacks)
                    if len(disregarded_attacks) > 0:
                        print(1)
                        for attack in disregarded_attacks:
                            print(2)
                            if attack['time_left'] < int(self.options['fleet']['save_time']):
                                print(3)
                                #first search and navigate to the planet under attack
                                for p in iter(self.planets):
                                    print(p.planet_coords)
                                    print(attack['dest_coords'])
                                    if p.planet_coords == attack['dest_coords']:
                                        print(5)
                                        #calculate the dest to the fleet and send all the fleet with all resources
                                        dest_coords = p.planet_coords.get_nearest(self.planets_coords)
                                        p.send_fleet(dest_coords,3,int(self.options['fleet']['speed']),all_fleet=True,all_resources=True)
                                        #add this attack to the already saved list
                                        self.fleet_saved.append(attack)
                            else:
                                self.logger.info("Attack Detected! its too early to fleet save")

            p.get_planet_resources()
            p.upgrade_resources()
            p.upgrade_stations()
            p.upgrade_researchs()
            #p.update_fleet()
            #p.build_ships()
            #p.build_defenses()

    def work(self):
        self.driver.switch_to_window(self.worker_tab)
        self.handle_planets()
        return True

    def unfleetsaved_attacks(self, attack_list):
        saned_list = []
        for attack in attack_list:
            if attack not in self.fleet_saved:
                saned_list.append(attack)
        return saned_list
