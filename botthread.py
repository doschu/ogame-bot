from threading import Thread
from bot import Bot

class BotThread(Thread):
    def __init__(self, user, password, server):
        Thread.__init__(self)
        self.user = user
        self.password = password
        self.server = server

    def run(self):
        bot = Bot(self.user, self.password, self.server)
        bot.start()
